﻿Public Class txtEspec
    'Variables
    Public CompSelected, CaseSelected, ComboUni_Cal, ComboUni_Fria As String

    Public CorrFriaF As Decimal
    Public CorrFriaT As Decimal
    Public CorrFriaP As Decimal
    Public CorrCalF As Decimal
    Public CorrCalT As Decimal
    Public CorrCalP As Decimal
    Public CorrCalVF As Decimal
    Public CorrCalT2 As Decimal
    Public Espec As Decimal

    Dim a(7), b(7), c(7), d(7), hc1, hc2, tf2, Q, MLDT, Area, U, cpva, tp, cpIndex, cpComp, pmComp, pm, cp25 As Decimal

    'pm agua para cpva
    Dim pma = 18





    'u 
    Public u1 = 450
    Public u2 = 450
    Public u3 = 75
    Public u4 = 150
    Public u5 = 150
    Public u6 = 150



    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboComp.SelectedIndexChanged

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub txtCc_vf_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub Label15_Click(sender As Object, e As EventArgs) Handles Label.Click

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Label9_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CompSelected = ComboComp.Text
        CaseSelected = ComboCase.Text
        ComboUni_Cal = CombUniCal.Text
        ComboUni_Fria = ComboUniFria.Text

        If CaseSelected = "Caso 1 (Fracción de vapor en la corriente caliente de salida)" Then
            CorrCalVF = Val(TextBox1.Text)

        ElseIf CaseSelected = "Caso 2 (temperatura en la corriente caliente de salida )" Then
            CorrCalT = Val(TextBox1.Text)

        ElseIf CaseSelected = "Caso 3 (temperatura en la corriente fria de salida)" Then
            CorrCalVF = Val(TextBox1.Text)

        End If

        'constantes cpf(T)
        'agua
        a(1) = 18.2964
        b(1) = 0.472118
        c(1) = -0.0013387
        d(1) = 0.00000131424
        'metanol
        a(2) = -258.25
        b(2) = 3.3582
        c(2) = -0.0116388
        d(2) = 0.0000140516
        'etanol
        a(3) = -325.137
        b(3) = 4.13787
        c(3) = -0.0140307
        d(3) = 0.0000170354
        'acetona
        a(4) = 16.8022
        b(4) = 0.848409
        c(4) = -0.00264114
        d(4) = 0.0000027825
        'benceno
        a(5) = -7.27329
        b(5) = 0.770541
        c(5) = -0.00164818
        d(5) = 0.00000189794
        'tolueno
        a(6) = 1.80826
        b(6) = 0.812223
        c(6) = -0.00151267
        d(6) = 0.00000163001





        'Seleccionar Cp y PM's
        Select Case CompSelected
            Case "Agua"
                cpIndex = 1
                'pm agua
                pm = 18
            Case "Metanol"
                cpIndex = 2
                'pm metanol
                pm = 16
            Case "Etanol"
                cpIndex = 3
                'pm etanol
                pm = 46
            Case "Acetona"
                cpIndex = 4
                'pm acetona
                pm = 58
            Case "Benceno"
                cpIndex = 5
                'pm benceno
                pm = 78
            Case "Tolueno"
                cpIndex = 6
                'pm tolueno
                pm = 92
        End Select





        CorrFriaF = Val(txtCf_f.Text)
        CorrFriaT = Val(txtCf_t.Text)
        CorrFriaP = Val(txtCf_p.Text)
        CorrCalF = Val(txtCc_f.Text)
        ' CorrCalT = Val(txtCc_t.Text)
        CorrCalP = Val(txtCc_p.Text)
        'CorrCalVF = Val(txtCc_vf.Text)
        Espec = Val(txtExpec.Text)


        If ComboUniFria.Text = "°F" Then
            CorrFriaT = ((CorrFriaT - 32) / 1.8) + 273.15
        ElseIf ComboUniFria.Text = "R" Then
            CorrFriaT = (CorrFriaT / 1.8)
        ElseIf ComboUniFria.Text = "°C" Then
            CorrFriaT = (CorrFriaT + 273.15)
        End If

        If CombUniCal.Text = "°F" Then
            CorrCalT = ((CorrCalT - 32) / 1.8) + 273.15
        ElseIf CombUniCal.Text = "R" Then
            CorrCalT = (CorrCalT / 1.8)
        ElseIf CombUniCal.Text = "°C" Then
            CorrCalT = (CorrCalT + 273.15)
        End If
        'T prom
        Select Case CaseSelected
            Case "Caso 1 (Fracción de vapor en la corriente caliente de salida)"
                hc1 = 0
                hc2 = 0
                tf2 = 0
                cpva = 0
                Q = 0
                MLDT = 0



                tp = CorrFriaT
                cpComp = a(cpIndex) + (b(cpIndex) * tp) + (c(cpIndex) * (tp ^ 2)) + (d(cpIndex) * (tp ^ 3))
                cpComp = cpComp / (pm * 4.1868)

                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32

                hc1 = 180.17 + CorrCalVF * (970.3) + cpva * (CorrCalT - 212)
                hc2 = 180.17 + (Espec * 970.3)

                tf2 = ((CorrCalF) * (hc1 - hc2)) / (CorrFriaF * cpComp) + CorrFriaT

                tp = (CorrFriaT + tf2) / 2
                tp = ((tp - 32) / 1.8) + 273.15
                cpComp = a(cpIndex) + (b(cpIndex) * tp) + (c(cpIndex) * (tp ^ 2)) + (d(cpIndex) * (tp ^ 3))
                cpComp = cpComp / (pm * 4.1868)

                CorrCalT = 212
                CorrCalT2 = 212

                tp = (CorrCalT + CorrCalT2) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma



                tf2 = CorrCalF * (hc1 - hc2) / (CorrFriaF * cpComp) + CorrFriaT

                Q = CorrFriaF * cpComp * (tf2 - CorrFriaT)
                MLDT = ((CorrCalT2 - CorrFriaT) - (CorrCalT - tf2)) / Math.Log((CorrCalT2 - CorrFriaT) / (CorrCalT - tf2))
                Area = Q / (MLDT * u5)

                MessageBox.Show("Tc2: " & CorrCalT2 & vbLf & "Tf2: " & tf2 & vbLf & "Area: " & Area)


            Case "Caso 2 (temperatura en la corriente caliente de salida )"

                tp = CorrFriaT
                cpComp = a(cpIndex) + b(cpIndex) * tp + (c(cpIndex) * (tp ^ 2)) + (d(cpIndex) * (tp ^ 3))
                cpComp = cpComp / (pm * 4.1868)
                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32
                tf2 = ((CorrCalF) * (hc1 - hc2)) / (CorrFriaF * cpComp) + CorrFriaT '°F
                tp = (CorrFriaT + tf2) / 2
                tp = ((tp - 32) / 1.8) + 273.5
                cpComp = a(cpIndex) + (b(cpIndex) * tp) + (c(cpIndex) * Math.Pow(tp, 2)) + (d(cpIndex) * Math.Pow(tp, 3))
                cpComp = cpComp / (pm * 4.1868)

                CorrCalVF = 1

                Dim CorrCalVF2 As Double
                CorrCalVF2 = 1
                CorrCalT = ((CorrCalT - 273.15) * 9 / 5) + 32

                tp = (Espec + 212) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                hc1 = 180.17 + (CorrCalVF * 970.3) + cpva * (CorrCalT - 212)
                hc2 = 180.17 + (CorrCalVF2 * 970.3) + cpva * (Espec - 212)

                tf2 = ((CorrCalF * (hc1 - hc2)) / (CorrFriaF * cpComp)) + CorrFriaT
                Q = CorrFriaF * cpComp * (tf2 - CorrFriaT)
                MLDT = ((Espec - CorrFriaT) - (CorrCalT - tf2)) / Math.Log((Espec - CorrFriaT) / (CorrCalT - tf2))
                Area = Q / (MLDT * u5)



                MessageBox.Show("Tf2: " & tf2 & vbLf & "VFc2: " & CorrCalVF2 & vbLf & "Area: " & Area)

            Case "Caso 3 (temperatura en la corriente fria de salida)"
                Espec = ((Espec - 32) / 1.8) + 273.15

                tp = (CorrFriaT + Espec) / 2
                cpComp = a(cpIndex) + (b(cpIndex) * tp) + (c(cpIndex) * Math.Pow(tp, 2)) + (d(cpIndex) * Math.Pow(tp, 3))
                cpComp = cpComp / (pm * 4.1868)

                'convertir
                Espec = ((Espec - 273.15) * 9 / 5) + 32
                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32

                Dim corrcalVF2 As Double
                corrcalVF2 = 1
                CorrCalT = 212

                tp = CorrFriaT
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma
                hc1 = 180.17 + (CorrCalVF * 970.3) + cpva * (CorrCalT - 212)
                hc2 = (((CorrFriaF * cpComp * (Espec - CorrFriaT)) / CorrCalF) - hc1) * -1
                CorrCalT2 = ((hc2 - 180.17 - (corrcalVF2 * 970.3)) / cpva) + 212

                If CorrCalT2 < 212 Then
                    CorrCalT2 = 212

                End If

                tp = (CorrFriaT + Espec) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                corrcalVF2 = ((hc2 - 180.17) - (cpva * (CorrCalT2 - 212))) / 970.3
                Q = CorrFriaF * cpComp * (Espec - CorrFriaT)

                MLDT = ((CorrCalT2 - CorrFriaT) - (CorrCalT - Espec)) / Math.Log((CorrCalT2 - CorrFriaT) / (CorrCalT - Espec))
                Area = Q / (MLDT * u5)

                MessageBox.Show("Tc2: " & CorrCalT2 & vbLf & "VFc2: " & corrcalVF2 & vbLf & "Area: " & Area)


        End Select
    End Sub

    Private Sub txtCc_t_KeyDown(sender As Object, e As KeyEventArgs)


    End Sub

    Private Sub txtCc_t_KeyPress(sender As Object, e As KeyPressEventArgs)

    End Sub

    Private Sub txtCc_t_MouseDown(sender As Object, e As MouseEventArgs)

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub GroupBox3_Enter(sender As Object, e As EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub Label12_Click(sender As Object, e As EventArgs) Handles Label12.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CompSelected = ComboComp.Text
        CaseSelected = ComboCase.Text
        ComboUni_Cal = CombUniCal.Text
        ComboUni_Fria = ComboUniFria.Text

        If CaseSelected = "Caso 1 (Fracción de vapor en la corriente caliente de salida)" Then
            CorrCalVF = Val(TextBox1.Text)

        ElseIf CaseSelected = "Caso 2 (temperatura en la corriente caliente de salida )" Then
            CorrCalT = Val(TextBox1.Text)

        ElseIf CaseSelected = "Caso 3 (temperatura en la corriente fria de salida)" Then
            CorrCalVF = Val(TextBox1.Text)

        End If

        CorrFriaF = Val(txtCf_f.Text)
        CorrFriaT = Val(txtCf_t.Text)
        CorrFriaP = Val(txtCf_p.Text)
        CorrCalF = Val(txtCc_f.Text)
        'CorrCalT = Val(txtCc_t.Text)
        CorrCalP = Val(txtCc_p.Text)
        'CorrCalVF = Val(txtCc_vf.Text)
        Espec = Val(txtExpec.Text)

        'unidades T
        '** Se utiliza en tambien en cp promedio debe de ir en una funcion
        If ComboUniFria.Text = "°F" Then
            CorrFriaT = (CorrFriaT - 32) / 1.8 + 273.15
        ElseIf ComboUniFria.Text = "R" Then
            CorrFriaT = (CorrFriaT / 1.8)
        ElseIf ComboUniFria.Text = "°C" Then
            CorrFriaT = (CorrFriaT + 273.15)
        End If
        If CombUniCal.Text = "°F" Then
            CorrCalT = (CorrCalT - 32) / 1.8 + 273.15
        ElseIf CombUniCal.Text = "R" Then
            CorrCalT = (CorrCalT / 1.8)
        ElseIf CombUniCal.Text = "°C" Then
            CorrCalT = (CorrCalT + 273.15)
        End If



        'Seleccionar cp a 25° y PM's
        Select Case CompSelected
            Case "Agua"
                cp25 = 1
                'pm agua
                pm = 18
            Case "Metanol"
                cp25 = 0.593
                'pm metanol
                pm = 16
            Case "Etanol"
                cp25 = 0.582
                'pm etanol
                pm = 46
            Case "Acetona"
                cp25 = 0.515
                'pm acetona
                pm = 58
            Case "Benceno"
                cp25 = 0.387
                'pm benceno
                pm = 78
            Case "Tolueno"
                cp25 = 0.397
                'pm tolueno
                pm = 92
        End Select


        'T prom

        Select Case CaseSelected
                    'LISTOOOOOOOOOOOOOOOOOOOOOOOOO
            Case "Caso 1 (Fracción de vapor en la corriente caliente de salida)"
                hc1 = 0
                hc2 = 0
                tf2 = 0
                cpva = 0
                Q = 0
                MLDT = 0

                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32



                CorrCalT = 212
                CorrCalT2 = 212

                tp = (CorrCalT - CorrCalT2) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                hc1 = 180.17 + CorrCalVF * (970.3) + cpva * (CorrCalT - 212)
                hc2 = 180.17 + (Espec * 970.3)

                tf2 = CorrCalF * (hc1 - hc2) / (CorrFriaF * cp25) + CorrFriaT

                Q = CorrFriaF * cp25 * (tf2 - CorrFriaT)
                MLDT = ((CorrCalT2 - CorrFriaT) - (CorrCalT - tf2)) / Math.Log((CorrCalT2 - CorrFriaT) / (CorrCalT - tf2))
                Area = Q / (MLDT * u5)

                MessageBox.Show("Tc2: " & CorrCalT2 & vbLf & "Tf2: " & tf2 & vbLf & "Area: " & Area)

            Case "Caso 2 (temperatura en la corriente caliente de salida )"
                'LISTOOOOOOOOOOOOOO
                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32
                CorrCalVF = 1

                Dim CorrCalVF2 As Double
                CorrCalVF2 = 1
                CorrCalT = ((CorrCalT - 273.15) * 9 / 5) + 32

                tp = (Espec + 212) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                hc1 = 180.17 + (CorrCalVF * 970.3) + cpva * (CorrCalT - 212)
                hc2 = 180.17 + (CorrCalVF2 * 970.3) + cpva * (Espec - 212)

                tf2 = ((CorrCalF * (hc1 - hc2)) / (CorrFriaF * cp25)) + CorrFriaT
                Q = CorrFriaF * cp25 * (tf2 - CorrFriaT)
                MLDT = ((Espec - CorrFriaT) - (CorrCalT - tf2)) / Math.Log((Espec - CorrFriaT) / (CorrCalT - tf2))
                Area = Q / (MLDT * u5)

                MessageBox.Show("Tf2: " & tf2 & vbLf & "VFc2: " & CorrCalVF2 & vbLf & "Area: " & Area)

                        'LISTOOOOOOOOOOOOOOOOOOOOOOOO
            Case "Caso 3 (temperatura en la corriente fria de salida)"
                CorrFriaT = ((CorrFriaT - 273.15) * 9 / 5) + 32
                Dim CorrCalVF2 As Double

                CorrCalVF2 = 1
                CorrCalT = 212
                tp = CorrCalT
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                hc1 = 180.17 + (CorrCalVF * 970.3) + cpva * (CorrCalT - 212)
                hc2 = (((CorrFriaF * cp25 * (Espec - CorrFriaT)) / CorrCalF) - hc1) * -1
                CorrCalT2 = ((hc2 - 180.17 - (CorrCalVF2 * 970.3)) / cpva) + 212

                If CorrCalT2 < 212 Then
                    CorrCalT2 = 212

                End If

                tp = (CorrFriaT + Espec) / 2
                cpva = 7.986 + 0.00046 * (tp) + 0.0000014 * Math.Pow(tp, 2) - 0.0000000006578 * Math.Pow(tp, 3)
                cpva = cpva / pma

                CorrCalVF2 = ((hc2 - 180.17) - (cpva * (CorrCalT2 - 212))) / 970.3
                Q = CorrFriaF * cp25 * (Espec - CorrFriaT)
                MLDT = ((CorrCalT2 - CorrFriaT) - (CorrCalT - Espec)) / Math.Log((CorrCalT2 - CorrFriaT) / (CorrCalT - Espec))
                Area = Q / (MLDT * u5)
                MessageBox.Show("Tc2: " & CorrCalT2 & vbLf & "VFc2: " & CorrCalVF2 & vbLf & "Area: " & Area)
        End Select

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub ComboCaso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboCase.SelectedIndexChanged
        CaseSelected = ComboCase.Text
        If CaseSelected = "Caso 1 (Fracción de vapor en la corriente caliente de salida)" Then
            Label9.Text = "VF"
            CombUniCal.Visible = False
        ElseIf CaseSelected = "Caso 2 (temperatura en la corriente caliente de salida )" Then
            Label9.Text = "T"
            CombUniCal.Visible = True
        ElseIf CaseSelected = "Caso 3 (temperatura en la corriente fria de salida)" Then
            Label9.Text = "VF"
            CombUniCal.Visible = False
        End If
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles txtCf_t.TextChanged

    End Sub

    Private Sub txtCc_vf_KeyPress(sender As Object, e As KeyPressEventArgs)

    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class txtEspec
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ComboComp = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label = New System.Windows.Forms.Label()
        Me.CombUniCal = New System.Windows.Forms.ComboBox()
        Me.ComboUniFria = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCc_p = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCc_f = New System.Windows.Forms.TextBox()
        Me.txtCf_p = New System.Windows.Forms.TextBox()
        Me.txtCf_t = New System.Windows.Forms.TextBox()
        Me.txtCf_f = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtExpec = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ComboCase = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboComp
        '
        Me.ComboComp.FormattingEnabled = True
        Me.ComboComp.Items.AddRange(New Object() {"Agua", "Metanol", "Etanol", "Acetona", "Benceno", "Tolueno"})
        Me.ComboComp.Location = New System.Drawing.Point(42, 66)
        Me.ComboComp.Name = "ComboComp"
        Me.ComboComp.Size = New System.Drawing.Size(167, 21)
        Me.ComboComp.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Seleccionar Componente:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Datos de Entrada"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox1.AutoSize = True
        Me.GroupBox1.Controls.Add(Me.ComboComp)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(66, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(268, 184)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Paso 1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label)
        Me.GroupBox2.Controls.Add(Me.CombUniCal)
        Me.GroupBox2.Controls.Add(Me.ComboUniFria)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtCc_p)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtCc_f)
        Me.GroupBox2.Controls.Add(Me.txtCf_p)
        Me.GroupBox2.Controls.Add(Me.txtCf_t)
        Me.GroupBox2.Controls.Add(Me.txtCf_f)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(54, 237)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(280, 184)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Paso 3"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(145, 137)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 13)
        Me.Label9.TabIndex = 26
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(165, 131)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(63, 21)
        Me.TextBox1.TabIndex = 25
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(230, 108)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(26, 13)
        Me.Label17.TabIndex = 24
        Me.Label17.Text = "psia"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(230, 82)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "lb/hr"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(101, 108)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(26, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "psia"
        '
        'Label
        '
        Me.Label.AutoSize = True
        Me.Label.Location = New System.Drawing.Point(99, 85)
        Me.Label.Name = "Label"
        Me.Label.Size = New System.Drawing.Size(29, 13)
        Me.Label.TabIndex = 21
        Me.Label.Text = "lb/hr"
        '
        'CombUniCal
        '
        Me.CombUniCal.FormattingEnabled = True
        Me.CombUniCal.Items.AddRange(New Object() {"K", "°F", "°C", "R"})
        Me.CombUniCal.Location = New System.Drawing.Point(230, 131)
        Me.CombUniCal.Name = "CombUniCal"
        Me.CombUniCal.Size = New System.Drawing.Size(37, 21)
        Me.CombUniCal.TabIndex = 20
        '
        'ComboUniFria
        '
        Me.ComboUniFria.FormattingEnabled = True
        Me.ComboUniFria.Items.AddRange(New Object() {"K", "°F", "°C", "R"})
        Me.ComboUniFria.Location = New System.Drawing.Point(95, 131)
        Me.ComboUniFria.MaximumSize = New System.Drawing.Size(37, 0)
        Me.ComboUniFria.Name = "ComboUniFria"
        Me.ComboUniFria.Size = New System.Drawing.Size(37, 21)
        Me.ComboUniFria.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(145, 108)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(14, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "P"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(145, 82)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(13, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "F"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 108)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(14, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "P"
        '
        'txtCc_p
        '
        Me.txtCc_p.Location = New System.Drawing.Point(165, 105)
        Me.txtCc_p.Multiline = True
        Me.txtCc_p.Name = "txtCc_p"
        Me.txtCc_p.Size = New System.Drawing.Size(63, 21)
        Me.txtCc_p.TabIndex = 12
        Me.txtCc_p.Text = "14.7"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "T"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "F"
        '
        'txtCc_f
        '
        Me.txtCc_f.Location = New System.Drawing.Point(165, 79)
        Me.txtCc_f.Multiline = True
        Me.txtCc_f.Name = "txtCc_f"
        Me.txtCc_f.Size = New System.Drawing.Size(63, 21)
        Me.txtCc_f.TabIndex = 10
        '
        'txtCf_p
        '
        Me.txtCf_p.Location = New System.Drawing.Point(29, 105)
        Me.txtCf_p.Multiline = True
        Me.txtCf_p.Name = "txtCf_p"
        Me.txtCf_p.Size = New System.Drawing.Size(63, 21)
        Me.txtCf_p.TabIndex = 9
        Me.txtCf_p.Text = "14.7"
        '
        'txtCf_t
        '
        Me.txtCf_t.Location = New System.Drawing.Point(29, 131)
        Me.txtCf_t.Multiline = True
        Me.txtCf_t.Name = "txtCf_t"
        Me.txtCf_t.Size = New System.Drawing.Size(63, 21)
        Me.txtCf_t.TabIndex = 8
        '
        'txtCf_f
        '
        Me.txtCf_f.Location = New System.Drawing.Point(29, 79)
        Me.txtCf_f.Multiline = True
        Me.txtCf_f.Name = "txtCf_f"
        Me.txtCf_f.Size = New System.Drawing.Size(63, 21)
        Me.txtCf_f.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "  Corriente Fria"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(154, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "  Corriente Caliente"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox3.Controls.Add(Me.txtExpec)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.ComboCase)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Location = New System.Drawing.Point(369, 11)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(268, 202)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Paso 2"
        '
        'txtExpec
        '
        Me.txtExpec.Location = New System.Drawing.Point(45, 134)
        Me.txtExpec.Name = "txtExpec"
        Me.txtExpec.Size = New System.Drawing.Size(76, 20)
        Me.txtExpec.TabIndex = 5
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(42, 118)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(79, 13)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Especificación:"
        '
        'ComboCase
        '
        Me.ComboCase.FormattingEnabled = True
        Me.ComboCase.Items.AddRange(New Object() {"Caso 1 (Fracción de vapor en la corriente caliente de salida)", "Caso 2 (temperatura en la corriente caliente de salida )", "Caso 3 (temperatura en la corriente fria de salida)"})
        Me.ComboCase.Location = New System.Drawing.Point(6, 72)
        Me.ComboCase.Name = "ComboCase"
        Me.ComboCase.Size = New System.Drawing.Size(256, 21)
        Me.ComboCase.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(39, 56)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(145, 13)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Seleccionar Caso De Estudio"
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox4.Controls.Add(Me.Button2)
        Me.GroupBox4.Controls.Add(Me.Button1)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Location = New System.Drawing.Point(363, 219)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(255, 202)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Paso 4"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(42, 112)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(166, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Cp f(T)"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(42, 83)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(166, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Cp a 25°"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(39, 50)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(169, 13)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Seleccionar opción termodinámica"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'txtEspec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 435)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "txtEspec"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboComp As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCc_p As TextBox
    Friend WithEvents txtCc_f As TextBox
    Friend WithEvents txtCf_p As TextBox
    Friend WithEvents txtCf_t As TextBox
    Friend WithEvents txtCf_f As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents ComboCase As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents txtExpec As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents ErrorProvider1 As ErrorProvider
    Friend WithEvents CombUniCal As ComboBox
    Friend WithEvents ComboUniFria As ComboBox
    Friend WithEvents Label As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label9 As Label
End Class
